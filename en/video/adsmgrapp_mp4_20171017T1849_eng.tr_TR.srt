﻿1
00:00:01,987 --> 00:00:06,865
[Reklam Yöneticisi uygulaması]

2
00:00:06,865 --> 00:00:09,877
İşletmenizi yönetmek için sürekli yoğun ve hareket halindesiniz.

3
00:00:09,877 --> 00:00:14,147
Artık iOS'ta kullanabileceğiniz Reklam Yöneticisi uygulaması ile

4
00:00:14,147 --> 00:00:18,406
Facebook reklamlarınızı mobil cihazınızda oluşturmanızı

5
00:00:18,406 --> 00:00:21,275
ve nerede olursanız olun yönetmenizi kolaylaştırdık.

6
00:00:21,275 --> 00:00:25,130
Böylece işletmeniz için önemli olan tüm insanlara

7
00:00:25,130 --> 00:00:26,504
Facebook'u kullandıkları zamanlarda erişebilirsiniz.

8
00:00:28,017 --> 00:00:31,714
Sadece birkaç dokunuşla istediğiniz yerden sonuçlarınızı yönetebilirsiniz.

9
00:00:31,714 --> 00:00:35,988
Reklamları durdurun ve başlatın, bütçe ve planınızı düzenleyin

10
00:00:35,988 --> 00:00:38,575
ve önemli bildirimler alın.

11
00:00:38,575 --> 00:00:41,651
Uygulamayı indirmek için Apple App Store'a gidin,

12
00:00:41,651 --> 00:00:45,180
Facebook reklamları hakkında daha fazla bilgi almak için ise bu sayfayı ziyaret edin.

13
00:00:45,180 --> 00:00:48,221
[facebook.com/business]

